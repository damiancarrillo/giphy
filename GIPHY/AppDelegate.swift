import Nuke
import GIPHYClient
import UIKit
import Willow

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    private let environment = AppEnvironment()
    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        prepareApplication()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = environment.makeRootViewController()
        window?.makeKeyAndVisible()
        return true
    }

    private func prepareApplication() {
        ImageLoadingOptions.shared.transition = .fadeIn(duration: 0.3)
        ImagePipeline.Configuration.isAnimatedImageDataEnabled = true
    }

}
