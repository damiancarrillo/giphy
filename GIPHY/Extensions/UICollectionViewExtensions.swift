import UIKit

public protocol ReusableCell: class {
    static var reuseIdentifier: String { get }
}

extension ReusableCell {
    public static var reuseIdentifier: String {
        return String(describing: self)
    }
}

public protocol ReuseableView: class {
    static var kind: String { get }
}

extension ReuseableView {
    public static var kind: String {
        return String(describing: self)
    }
}

extension UICollectionReusableView: ReusableCell {}
extension UICollectionReusableView: ReuseableView {}

public protocol ReusableCellHosting {
    func register<C: ReusableCell>(cellClass: C.Type)
    func dequeueCell<C: ReusableCell>(ofType cellClass: C.Type, for indexPath: IndexPath) -> C
    func dequeueCell<C: ReusableCell>(with identifier: String, for indexPath: IndexPath) -> C
}

extension UICollectionView {

    // MARK: Cells

    public func dequeueCell<C: ReusableCell>(ofType cellClass: C.Type, for indexPath: IndexPath) -> C {
        return dequeueReusableCell(withReuseIdentifier: cellClass.reuseIdentifier, for: indexPath) as! C
    }

    public func dequeueCell<C: ReusableCell>(with identifier: String, for indexPath: IndexPath) -> C {
        return dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! C
    }

    public func register<C: ReusableCell>(cellClass: C.Type) {
        self.register(cellClass, forCellWithReuseIdentifier: cellClass.reuseIdentifier)
    }

    public func registerNib<C: ReusableCell>(for cellClass: C.Type, bundle: Bundle? = nil) {
        self.register(
            UINib(nibName: cellClass.reuseIdentifier, bundle: bundle ?? Bundle(for: C.self)),
            forCellWithReuseIdentifier: cellClass.reuseIdentifier
        )
    }

    // MARK: Supplementary views

    public func register<C: ReusableCell>(cellClass: C.Type, forKind kind: String) {
        self.register(cellClass, forSupplementaryViewOfKind: kind, withReuseIdentifier: cellClass.reuseIdentifier)
    }

    public func registerNib<C: ReusableCell>(for cellClass: C.Type, forSupplementaryViewOfKind kind: String, bundle: Bundle? = nil) {
        self.register(
            UINib(nibName: cellClass.reuseIdentifier, bundle: bundle ?? Bundle(for: C.self)),
            forSupplementaryViewOfKind: kind, withReuseIdentifier: cellClass.reuseIdentifier
        )
    }

    public func dequeueSupplementaryView<C: ReusableCell>(ofKind kind: String, ofType cellClass: C.Type, for indexPath: IndexPath) -> C {
        return self.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: cellClass.reuseIdentifier, for: indexPath) as! C
    }
}
