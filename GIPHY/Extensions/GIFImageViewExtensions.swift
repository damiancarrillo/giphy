import Gifu
import Nuke
import UIKit

extension GIFImageView {
    public override func display(image: Image?) {
        prepareForReuse()
        if let data = image?.animatedImageData {
            animate(withGIFData: data)
        } else {
            self.image = image
        }
    }
}
