import UIKit

enum ColorScheme {

    static let green  = { UIColor(red:   0.0 / 255.0, green: 255.0 / 255.0, blue: 153.0 / 255.0, alpha: 1.0) }()
    static let blue   = { UIColor(red:   0.0 / 255.0, green: 204.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0) }()
    static let red    = { UIColor(red: 255.0 / 255.0, green: 102.0 / 255.0, blue: 102.0 / 255.0, alpha: 1.0) }()
    static let yellow = { UIColor(red: 255.0 / 255.0, green: 243.0 / 255.0, blue:  92.0 / 255.0, alpha: 1.0) }()
    static let purple = { UIColor(red: 153.0 / 255.0, green:  51.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0) }()

    static let background = { UIColor.black }()
    static let foreground = { UIColor.white }()
    static let buttonDestructive = red

    static let primaryColors = { [green, red, yellow, blue, purple] }()
    
}
