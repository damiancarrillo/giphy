import GIPHYClient
import Nuke
import RxSwift
import UIKit
import Willow

final class RootViewController: UIViewController {

    private let disposeBag = DisposeBag()
    private let logger: Logger
    private let logoImageView = UIImageView()
    private let makeSearchViewController: SearchViewControllerFactory
    private let randomEndpoint: RandomEndpoint

    init(logger: Logger, randomEndpoint: RandomEndpoint, makeSearchViewController: @escaping SearchViewControllerFactory) {
        self.logger = logger
        self.randomEndpoint = randomEndpoint
        self.makeSearchViewController = makeSearchViewController
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        view.addSubview(logoImageView)

        logoImageView.image = #imageLiteral(resourceName: "LaunchLogo")
        logoImageView.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            logoImageView.widthAnchor.constraint(equalToConstant: 256),
            logoImageView.heightAnchor.constraint(equalToConstant: 256),
            logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            logoImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
        ])
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        let searchViewController = makeSearchViewController()
        let navigationController = UINavigationController(rootViewController: searchViewController)

        do {
            try randomEndpoint.random(parameters: RandomParameters(tag: "giphy"))
                .observeOn(MainScheduler.instance)
                .timeout(3, scheduler: MainScheduler.instance)
                .subscribe(onSuccess: { [weak self] result in
                    guard let imageURL = result.1.data.images.downsizedMedium?.url else { return }

                    Nuke.loadImage(
                        with: imageURL,
                        options: ImageLoadingOptions.shared,
                        into: searchViewController.placeholderImageView,
                        progress: nil,
                        completion: { response, error in
                            self?.transitionToMainViewController(navigationController)
                        }
                    )
                }, onError: { [weak self] error in
                    self?.transitionToMainViewController(navigationController)
                })
                .disposed(by: disposeBag)
        } catch {
            logger.errorMessage("Failed to fetch placeholder image: \(error)")
            transitionToMainViewController(navigationController)
        }
    }

    private func transitionToMainViewController(_ mainViewController: UIViewController) {
        mainViewController.modalPresentationStyle = .overCurrentContext
        mainViewController.modalTransitionStyle = .crossDissolve
        present(mainViewController, animated: true, completion: nil)
    }

}

