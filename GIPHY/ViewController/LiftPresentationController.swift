import Metron
import RxCocoa
import RxSwift
import UIKit

final class LiftPresentationController: UIPresentationController {

    private let collectionView: UICollectionView
    private let dimmingView = UIView()
    private let disposeBag = DisposeBag()
    private let impactFeedbackGenerator = UIImpactFeedbackGenerator(style: .medium)
    private let indexPath: IndexPath
    private let tapGestureRecognizer = UITapGestureRecognizer()

    init(
        presentedViewController: UIViewController,
        presenting: UIViewController?,
        collectionView: UICollectionView,
        indexPath: IndexPath
    ) {
        self.collectionView = collectionView
        self.indexPath = indexPath

        super.init(presentedViewController: presentedViewController, presenting: presenting)

        tapGestureRecognizer.rx.event
            .bind(onNext: { _ in
                presentedViewController.dismiss(animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
    }

    public override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()

        guard
            let containerView = containerView,
            let layoutAttributes = collectionView.layoutAttributesForItem(at: indexPath),
            let cell = collectionView.cellForItem(at: indexPath) as? SearchResultsCell,
            let image = cell.imageView.image
            else { return }

        let sourceFrame = collectionView.convert(layoutAttributes.frame, to: containerView)

        impactFeedbackGenerator.prepare()

        containerView.addGestureRecognizer(tapGestureRecognizer)
        containerView.addSubview(dimmingView)
        containerView.addSubview(cell.imageView)

        dimmingView.backgroundColor = ColorScheme.background
        dimmingView.translatesAutoresizingMaskIntoConstraints = false
        dimmingView.alpha = 0
        
        NSLayoutConstraint.activate([
            dimmingView.topAnchor.constraint(equalTo: containerView.topAnchor),
            dimmingView.leftAnchor.constraint(equalTo: containerView.leftAnchor),
            dimmingView.rightAnchor.constraint(equalTo: containerView.rightAnchor),
            dimmingView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
        ])

        cell.imageView.frame = sourceFrame
        cell.imageView.translatesAutoresizingMaskIntoConstraints = true

        presentingViewController.transitionCoordinator?.animate(alongsideTransition: { [dimmingView] context in
            dimmingView.alpha = 1
            cell.imageView.frame = CGRect(aspectFitSize: image.size, inRect: containerView.bounds)
        }, completion: { [presentedViewController] context in
            guard let presentedViewController = presentedViewController as? SearchResultsViewController else {
                return
            }
            presentedViewController.view.addSubview(cell.imageView)
        })

        impactFeedbackGenerator.impactOccurred()
    }

    public override func dismissalTransitionWillBegin() {
        super.dismissalTransitionWillBegin()

        guard
            let containerView = containerView,
            let layoutAttributes = collectionView.layoutAttributesForItem(at: indexPath),
            let cell = collectionView.cellForItem(at: indexPath) as? SearchResultsCell
            else { return }

        containerView.addSubview(cell.imageView)

        let sourceFrame = collectionView.convert(layoutAttributes.frame, to: containerView)

        presentingViewController.transitionCoordinator?.animate(alongsideTransition: { [dimmingView] context in
            dimmingView.alpha = 0
            cell.imageView.frame = sourceFrame
        }, completion: nil)

        impactFeedbackGenerator.impactOccurred()
    }

    override func dismissalTransitionDidEnd(_ completed: Bool) {
        super.dismissalTransitionDidEnd(completed)

        guard let cell = collectionView.cellForItem(at: indexPath) as? SearchResultsCell else { return }

        cell.reinsertImageView()

        if !completed {
            dimmingView.removeFromSuperview()
        }
    }

    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)

        guard
            let containerView = containerView,
            let cell = collectionView.cellForItem(at: indexPath) as? SearchResultsCell,
            let image = cell.imageView.image
            else { return }

        coordinator.animate(alongsideTransition: { context in
            cell.imageView.frame = CGRect(aspectFitSize: image.size, inRect: containerView.bounds)
        }, completion: nil)
    }

}
