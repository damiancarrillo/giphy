import Gifu
import GIPHYClient
import RxCocoa
import RxSwift
import UIKit

final class AnimatedGIFViewController: UIViewController {

    var imageView: GIFImageView?

    private let closeButton = UIButton(type: .system)
    private let disposeBag = DisposeBag()
    private let gif: GIF

    init(gif: GIF) {
        self.gif = gif
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        closeButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(closeButton)

        NSLayoutConstraint.activate([
            closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10),
            closeButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10),
        ])

        closeButton.setTitle("Close", for: .normal)
        closeButton.rx.tap
            .bind(onNext: {
                self.dismiss(animated: true, completion: nil)
            })
            .disposed(by: disposeBag)

        applyTheme()
    }

    private func applyTheme() {
        closeButton.tintColor = ColorScheme.buttonDestructive
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
