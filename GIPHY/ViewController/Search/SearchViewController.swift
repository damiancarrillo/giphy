import AVFoundation
import Gifu
import GIPHYClient
import Nuke
import RxSwift
import RxSwiftExt
import UIKit
import Willow

final class SearchViewController: UIViewController {

    public var placeholderImageView = GIFImageView(image: nil)
    
    private let disposeBag = DisposeBag()
    private let impactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
    private let logger: Logger
    private var placeholderIndex = 0
    private let randomEndpoint: RandomEndpoint
    private let searchController: UISearchController
    private let searchEndpoint: SearchEndpoint
    private let searchResultsViewController: SearchResultsViewController
    private var spacing = CGFloat(8)

    init(logger: Logger, randomEndpoint: RandomEndpoint, searchEndpoint: SearchEndpoint) {
        self.searchResultsViewController = SearchResultsViewController(collectionViewLayout: UICollectionViewFlowLayout())
        self.searchController = UISearchController(searchResultsController: searchResultsViewController)
        self.randomEndpoint = randomEndpoint
        self.searchEndpoint = searchEndpoint
        self.logger = logger

        super.init(nibName: nil, bundle: nil)

        title = "GIPHY"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(placeholderImageView)

        prepareSearchController()
        preparePlaceholderImageView()
        applyTheme()
        beginObservingSearchParameters()
    }

    private func prepareSearchController() {
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.autocapitalizationType = .none
        searchController.delegate = self
    }

    private func udpatePlaceholder() {
        do {
            try randomEndpoint.random(parameters: RandomParameters(tag: "giphy"))
                .observeOn(MainScheduler.instance)
                .subscribe(onSuccess: { [placeholderImageView] result in
                    guard let imageURL = result.1.data.images.downsizedMedium?.url else { return }

                    let imageRequest = ImageRequest(
                        url: imageURL,
                        targetSize: placeholderImageView.bounds.size,
                        contentMode: .aspectFill,
                        upscale: true
                    )

                    Nuke.loadImage(with: imageRequest, into: placeholderImageView, progress: nil)
                })
                .disposed(by: disposeBag)
        } catch {
            logger.errorMessage("Error updating placeholder: \(error)")
        }
    }

    private func preparePlaceholderImageView() {
        placeholderImageView.translatesAutoresizingMaskIntoConstraints = false
        placeholderImageView.contentMode = .scaleAspectFill
        placeholderImageView.alpha = 0.5

        NSLayoutConstraint.activate([
            placeholderImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: view.directionalLayoutMargins.top),
            placeholderImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: view.directionalLayoutMargins.bottom),
            placeholderImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: view.directionalLayoutMargins.leading),
            placeholderImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: view.directionalLayoutMargins.trailing)
        ])
    }

    private func applyTheme() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = ColorScheme.foreground
        navigationController?.navigationBar.barStyle = .black

        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = ColorScheme.buttonDestructive

        searchController.searchBar.barStyle = .black
        searchController.searchBar.keyboardAppearance = .dark

        view.backgroundColor = ColorScheme.background
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private func beginObservingSearchParameters() {
        let searchQuery = searchController.searchBar.rx.text
            .orEmpty
            .map({ $0.trimmingCharacters(in: .whitespacesAndNewlines) })
            .filter({ !$0.isEmpty })
            .distinctUntilChanged()
            .throttle(1, latest: true, scheduler: MainScheduler.instance)

        let searchParameters = searchQuery
            .map({ SearchParameters(query: $0, limit: 100) })

        let search = searchParameters
            .flatMapLatest({ [searchEndpoint] in
                try searchEndpoint.search(parameters: $0)
            })
            .share()

        search
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [searchEndpoint, searchResultsViewController, navigationController] in
                searchResultsViewController.dataSource = SearchResultsDataSource(
                    searchEndpoint: searchEndpoint,
                    parameters: $0.0,
                    result: $0.1
                )
                searchResultsViewController.collectionView.reloadData()
                searchResultsViewController.collectionView.setContentOffset(.zero, animated: false)
                navigationController?.setNavigationBarHidden(true, animated: true)
            }, onError: { [logger] in
                logger.errorMessage($0.localizedDescription)
            })
            .disposed(by: disposeBag)

        // Flash the scroll indicators on the next turn of the run loop so the collection view is already laid out
        search
            .observeOn(MainScheduler.asyncInstance)
            .subscribe({ [searchResultsViewController] _ in
                searchResultsViewController.collectionView.flashScrollIndicators()
            })
            .disposed(by: disposeBag)
    }

}

extension SearchViewController: UISearchControllerDelegate {

    func willPresentSearchController(_ searchController: UISearchController) {
        impactFeedbackGenerator.impactOccurred()
    }

    func willDismissSearchController(_ searchController: UISearchController) {
        impactFeedbackGenerator.impactOccurred()
    }

}
