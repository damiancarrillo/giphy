import GIPHYClient
import UIKit

final class SearchResultsViewController: UICollectionViewController {

    var dataSource: SearchResultsDataSource? {
        didSet {
            if let dataSource = dataSource {
                collectionView.dataSource = dataSource
                collectionView.prefetchDataSource = dataSource
            } else {
                collectionView.dataSource = nil
                collectionView.prefetchDataSource = nil
            }
        }
    }

    private var spacing = CGFloat(8)
    private var flowLayout: UICollectionViewFlowLayout? { return collectionViewLayout as? UICollectionViewFlowLayout }
    private var liftPresentation: LiftPresentationController?

    override func viewDidLoad() {
        super.viewDidLoad()

        edgesForExtendedLayout = []
        collectionView.delaysContentTouches = true
        collectionView.keyboardDismissMode = .onDrag
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cellClass: SearchResultsCell.self)

        flowLayout?.sectionInset = UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: spacing)
        flowLayout?.minimumLineSpacing = spacing
        flowLayout?.minimumInteritemSpacing = spacing

        let length = min(view.bounds.midX, view.bounds.midY) - 1.5 * spacing
        flowLayout?.itemSize = CGSize(width: length, height: length)
    }

    private func applyTheme() {
        collectionView.backgroundColor = ColorScheme.background
        collectionView.indicatorStyle = .white

        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = ColorScheme.foreground
        navigationController?.navigationBar.barStyle = .black

        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = ColorScheme.buttonDestructive

        view.backgroundColor = ColorScheme.background
    }

    // MARK: - UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let gif = dataSource?.itemAtIndexPath(indexPath) else { return }
        
        let gifViewController = AnimatedGIFViewController(gif: gif)

        liftPresentation = LiftPresentationController(
            presentedViewController: gifViewController,
            presenting: self,
            collectionView: collectionView,
            indexPath: indexPath
        )

        gifViewController.modalPresentationCapturesStatusBarAppearance = true
        gifViewController.modalPresentationStyle = .custom
        gifViewController.modalTransitionStyle = .crossDissolve
        gifViewController.transitioningDelegate = self

        present(gifViewController, animated: true, completion: nil)
    }

}

extension SearchResultsViewController: UIViewControllerTransitioningDelegate {

    func presentationController(
        forPresented presented: UIViewController,
        presenting: UIViewController?,
        source: UIViewController
    ) -> UIPresentationController? {
        return liftPresentation
    }

}
