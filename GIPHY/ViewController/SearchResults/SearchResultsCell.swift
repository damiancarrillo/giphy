import GIPHYClient
import Gifu
import Nuke
import UIKit

final class SearchResultsCell: UICollectionViewCell {

    let imageView = GIFImageView(image: nil)
    private let highlightView = UIView()

    public var gif: GIF? {
        didSet {
            guard
                let gif = gif,
                let imageURL = gif.images.fixedWidth?.url
                else { return }

            Nuke.loadImage(with: imageURL, into: imageView)
        }
    }

    override var isHighlighted: Bool {
        didSet {
            highlightView.backgroundColor = primaryColor

            // highlighting should be immediate, unhighlighting should be gradual

            if isHighlighted {
                highlightView.alpha = 0.65
            } else {
                UIView.animate(withDuration: 0.2, delay: 0, options: .beginFromCurrentState, animations: {
                    self.highlightView.alpha = 0
                }, completion: nil)
            }

            highlightView.alpha = isHighlighted ? 0.65 : 0
        }
    }

    private var primaryColor: UIColor {
        return ColorScheme.primaryColors[tag % (ColorScheme.primaryColors.count)]
    }

    override var tag: Int {
        didSet {
            backgroundColor = primaryColor
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeCell()
    }

    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        initializeCell()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.prepareForReuse()
    }

    private func initializeCell() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true

        highlightView.translatesAutoresizingMaskIntoConstraints = false
        highlightView.alpha = 0

        contentView.addSubview(imageView)
        contentView.addSubview(highlightView)

        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

            highlightView.topAnchor.constraint(equalTo: contentView.topAnchor),
            highlightView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            highlightView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            highlightView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }

    public func reinsertImageView(){
        initializeCell()
    }

}
