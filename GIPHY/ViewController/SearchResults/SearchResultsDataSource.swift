import UIKit
import GIPHYClient
import RxCocoa
import RxSwift

final class SearchResultsDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDataSourcePrefetching {

    private let disposeBag = DisposeBag()
    private let endpoint: SearchEndpoint
    private var gifs: [GIF?]
    private let gifSubject = PublishSubject<(Int, GIF)>()
    private var parameters: SearchParameters
    private let mostRecentResult: SearchResult
    private let backgroundQueue: DispatchQueue

    init(
        searchEndpoint: SearchEndpoint,
        parameters: SearchParameters,
        result: SearchResult,
        backgroundQueue: DispatchQueue = DispatchQueue.global(qos: .userInitiated)
    ) {
        self.endpoint = searchEndpoint
        self.parameters = parameters
        self.mostRecentResult = result
        self.backgroundQueue = backgroundQueue

        self.gifs = Array(repeating: .none, count: result.pagination.totalCount)
        self.gifs.replaceSubrange(0..<result.data.count, with: result.data)

        super.init()
    }

    func itemAtIndexPath(_ indexPath: IndexPath) -> GIF? {
        return gifs[indexPath.item]
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gifs.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueCell(ofType: SearchResultsCell.self, for: indexPath)
        cell.tag = indexPath.item

        if let gif = gifs[indexPath.item] {
            cell.gif = gif
        } else {
            cell.gif = nil
            backgroundQueue.async { [weak self] in
                guard let self = self else { return }

                self.gifSubject
                    .filter({ $0.0 == indexPath.item })
                    .observeOn(MainScheduler.asyncInstance)
                    .subscribe(onNext: { (item, gif) in
                        if cell.tag == item {
                            cell.gif = gif
                        }
                    })
                    .disposed(by: self.disposeBag)
            }
        }

        return cell
    }



    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        let someGIFsNeedToBeFetched = !indexPaths
            .map({ [gifs] indexPath in gifs[indexPath.item + mostRecentResult.pagination.offset] })
            .filter({ $0 == nil })
            .isEmpty

        guard someGIFsNeedToBeFetched else { return }

        backgroundQueue.async { [weak self] in
            guard let self = self else { return }

            for page in Set(indexPaths.map({ $0.item / self.mostRecentResult.pagination.count })) {
                var parameters = self.parameters
                parameters.offset = page * parameters.limit

                do {
                    try self.endpoint.search(parameters: parameters).subscribe(
                        onSuccess: { parameters, result in
                            let start = parameters.offset ?? 0
                            let end = (parameters.offset ?? 0) + result.data.count

                            DispatchQueue.main.sync {
                                self.gifs.replaceSubrange(start..<end, with: result.data)
                            }

                            for (i, gif) in result.data.enumerated() {
                                self.gifSubject.onNext((i + (parameters.offset ?? 0), gif))
                            }
                        })
                        .disposed(by: self.disposeBag)
                } catch {
                    // log this
                }
            }
        }
    }

}
