import GIPHYClient
import Willow

typealias LoggerFactory = () -> Logger
typealias RootViewControllerFactory = () -> RootViewController
typealias SearchEndpointFactory = () -> SearchEndpoint
typealias SearchViewControllerFactory = () -> SearchViewController

class AppEnvironment {

    private let apiKey = "pQdO4RMfn707wjTAtJs7pN2sJJCpQz8v"
    private let logger = Logger(logLevels: [.all], writers: [ConsoleWriter()])

    // MARK: - Client

    lazy var makeRandomEndpoint: () -> RandomEndpoint = {
        return {
            RandomEndpointImpl(apiKey: self.apiKey, logger: self.logger)
        }
    }()

    lazy var makeSearchEndpoint: () -> SearchEndpoint = {
        return {
            SearchEndpointImpl(apiKey: self.apiKey, logger: self.logger)
        }
    }()

    // MARK: - View Controllers

    var makeRootViewController: RootViewControllerFactory {
        return {
            RootViewController(
                logger: self.logger,
                randomEndpoint: self.makeRandomEndpoint(),
                makeSearchViewController: self.makeSearchViewController
            )
        }
    }

    var makeSearchViewController: SearchViewControllerFactory {
        return {
            SearchViewController(
                logger: self.logger,
                randomEndpoint: self.makeRandomEndpoint(),
                searchEndpoint: self.makeSearchEndpoint()
            )
        }
    }
    
}
