import Foundation

public enum Client {

    public static var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }

    public enum Error: Swift.Error {
        case bodyDecodingFailure(Any, HTTPURLResponse)
        case invalidRequestURL(URLComponents)
    }

}

protocol URLQueryItemsConvertible {
    var queryItems: [URLQueryItem] { get }
}

protocol RequestParameters: URLQueryItemsConvertible, Equatable {

}

protocol DecodedResponse: Decodable {
    
}
