import Foundation
import RxCocoa
import RxSwift
import Willow

public class BaseEndpoint {

    let apiKey: String
    let baseComponents: URLComponents
    let decoder: JSONDecoder
    let logger: Logger?
    let session: URLSession

    public init(session: URLSession = .shared, apiKey: String, logger: Logger?) {
        self.session = session
        self.apiKey = apiKey
        self.logger = logger
        self.decoder = JSONDecoder()
        self.decoder.keyDecodingStrategy = .convertFromSnakeCase
        self.decoder.dateDecodingStrategy = .iso8601

        var baseComponents = URLComponents()
        baseComponents.scheme = "https"
        baseComponents.host = "api.giphy.com"
        self.baseComponents = baseComponents
    }

    func makeRequest<P: RequestParameters, R: DecodedResponse>(path: String, parameters: P) throws -> Single<(P, R)> {
        var requestComponents = baseComponents
        requestComponents.path = path
        requestComponents.queryItems = [URLQueryItem(name: "api_key", value: apiKey)] + parameters.queryItems

        guard let requestURL = requestComponents.url else {
            throw Client.Error.invalidRequestURL(requestComponents)
        }

        logger?.logMessage({ "Making request to \(requestURL.absoluteString)" }, with: .info)

        return session.rx
            .data(request: URLRequest(url: requestURL))
            .map({ [decoder, logger] body in
                do {
                    let response = try decoder.decode(R.self, from: body)
                    return (parameters, response)
                } catch {
                    logger?.logMessage({ "Error decoding response for \(requestURL.absoluteString): \(error)" }, with: .error)
                    throw error
                }
            })
            .asSingle()
    }

}
