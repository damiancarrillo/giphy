import RxSwift

public protocol RandomEndpoint {
    func random(parameters: RandomParameters) throws -> Single<(RandomParameters, RandomResult)>
}
