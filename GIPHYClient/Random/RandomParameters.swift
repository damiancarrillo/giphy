import Foundation

public struct RandomParameters {
    let tag: String
    let rating: String?
    let fmt: String?

    public init(tag: String, rating: String? = nil, fmt: String? = nil) {
        self.tag = tag
        self.rating = rating
        self.fmt = fmt
    }
}

extension RandomParameters: RequestParameters {
    var queryItems: [URLQueryItem] {
        var queryItems = [
            URLQueryItem(name: "tag", value: tag)
        ]
        if let rating = rating {
            queryItems.append(URLQueryItem(name: "rating", value: rating))
        }
        if let fmt = fmt {
            queryItems.append(URLQueryItem(name: "fmt", value: fmt))
        }
        return queryItems
    }
}
