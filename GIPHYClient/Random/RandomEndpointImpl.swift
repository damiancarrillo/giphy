import Foundation
import RxSwift

public final class RandomEndpointImpl: BaseEndpoint, RandomEndpoint {

    public func random(parameters: RandomParameters) throws -> Single<(RandomParameters, RandomResult)> {
        return try makeRequest(path: "/v1/gifs/random", parameters: parameters)
    }

}
