public struct RandomResult: DecodedResponse {
    public let data: GIF
    public let meta: Meta
}
