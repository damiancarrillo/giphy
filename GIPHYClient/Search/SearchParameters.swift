import Foundation

public struct SearchParameters {

    public let query: String
    public let limit: Int
    public var offset: Int?
    public let rating: String?
    public let lang: String?
    public let fmt: String?

    public init(query: String, limit: Int, offset: Int? = nil, rating: String? = nil, lang: String? = nil, fmt: String? = nil) {
        self.query = query
        self.limit = limit
        self.offset = offset
        self.rating = rating
        self.lang = lang
        self.fmt = fmt
    }

    var nextPage: SearchParameters {
        return SearchParameters(query: query, limit: limit, offset: (offset ?? 0) + limit, rating: rating, lang: lang, fmt: fmt)
    }

}

extension SearchParameters: RequestParameters {
    var queryItems: [URLQueryItem] {
        var queryItems = [
            URLQueryItem(name: "q", value: query),
            URLQueryItem(name: "limit", value: String(limit))
        ]
        if let offset = offset {
            queryItems.append(URLQueryItem(name: "offset", value: String(offset)))
        }
        if let rating = rating {
            queryItems.append(URLQueryItem(name: "rating", value: rating))
        }
        if let lang = lang {
            queryItems.append(URLQueryItem(name: "lang", value: lang))
        }
        if let fmt = fmt {
            queryItems.append(URLQueryItem(name: "fmt", value: fmt))
        }
        return queryItems
    }
}
