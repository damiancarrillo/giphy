import Foundation
import RxSwift

public final class SearchEndpointImpl: BaseEndpoint, SearchEndpoint {

    public func search(parameters: SearchParameters) throws -> Single<(SearchParameters, SearchResult)> {
        return try makeRequest(path: "/v1/gifs/search", parameters: parameters)
    }

}
