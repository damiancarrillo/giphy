import RxSwift

public protocol SearchEndpoint {
    func search(parameters: SearchParameters) throws -> Single<(SearchParameters, SearchResult)>
}
