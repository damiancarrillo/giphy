public struct Pagination: Decodable {
    public let offset: Int
    public let totalCount: Int
    public let count: Int
}
