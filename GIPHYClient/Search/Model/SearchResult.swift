public struct SearchResult: DecodedResponse {
    public let data: [GIF]
    public let pagination: Pagination
    public let meta: Meta
}
