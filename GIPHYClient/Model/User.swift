import Foundation

public struct User: Decodable {
    public let avatarURL: URL?
    public let bannerURL: URL?
    public let profileURL: URL?
    public let username: String?
    public let displayName: String?
    public let twitter: String?

    private enum CodingKeys: String, CodingKey {
        case avatarURL = "avatar_url"
        case bannerURL = "banner_url"
        case profileURL = "profile_url"
        case username = "username"
        case displayName = "display_name"
        case twitter = "twitter"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.avatarURL = try container.decodeIfPresent(String.self, forKey: .avatarURL).flatMap({ URL(string: $0) })
        self.bannerURL = try container.decodeIfPresent(String.self, forKey: .bannerURL).flatMap({ URL(string: $0) })
        self.profileURL = try container.decodeIfPresent(String.self, forKey: .profileURL).flatMap({ URL(string: $0) })
        self.username = try container.decodeIfPresent(String.self, forKey: .username)
        self.displayName = try container.decodeIfPresent(String.self, forKey: .displayName)
        self.twitter = try container.decodeIfPresent(String.self, forKey: .twitter)
    }
}
