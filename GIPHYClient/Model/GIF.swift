import Foundation

public enum Rating: String, Decodable {
    case youth = "y"
    case generalAudiences = "g"
    case parentalGuidance = "pg"
    case parentalGuidance13 = "pg-13"
    case restricted = "r"
}

public struct GIF: Decodable {

    public let analytics: Analytics?
    public let bitlyGIFURL: URL?
    public let bitlyURL: URL?
    public let embedURL: URL?
    public let id: String
    public let images: Images
    public let importDateTime: Date?
    public let isSticker: Bool
    public let rating: Rating?
    public let score: Double?
    public let slug: String
    public let source: URL?
    public let sourcePostURL: URL?
    public let sourceTLD: String?
    public let title: String
    public let trendingDateTime: Date?
    public let type: String
    public let url: URL?
    public let user: User?
    public let username: String

    private enum CodingKeys: String, CodingKey {
        case analytics = "analytics"
        case bitlyGIFURL = "bitly_gif_url"
        case bitlyURL = "bitly_url"
        case embedURL = "embed_url"
        case id = "id"
        case images = "images"
        case importDateTime = "import_datetime"
        case isSticker = "is_sticker"
        case rating = "rating"
        case score = "_score"
        case slug = "slug"
        case source = "source"
        case sourcePostURL = "source_post_url"
        case sourceTLD = "source_tld"
        case title = "title"
        case trendingDateTime = "trending_datetime"
        case type = "type"
        case url = "url"
        case user = "user"
        case username = "username"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.analytics = try container.decodeIfPresent(Analytics.self, forKey: .analytics)
        self.bitlyGIFURL = try container.decodeIfPresent(String.self, forKey: .bitlyGIFURL).flatMap({ URL(string: $0) })
        self.bitlyURL = try container.decodeIfPresent(String.self, forKey: .bitlyURL).flatMap({ URL(string: $0 )})
        self.embedURL = try container.decodeIfPresent(String.self, forKey: .embedURL).flatMap({ URL(string: $0) })
        self.id = try container.decode(String.self, forKey: .id)
        self.images = try container.decode(Images.self, forKey: .images)
        self.importDateTime = try container.decodeIfPresent(Date.self, forKey: .importDateTime)
        self.isSticker = try container.decodeIfPresent(Int.self, forKey: .isSticker).flatMap({ $0 != 0 }) ?? false
        self.rating = try container.decodeIfPresent(Rating.self, forKey: .rating)
        self.score = try container.decodeIfPresent(Double.self, forKey: .score).flatMap({ Double($0) })
        self.slug = try container.decode(String.self, forKey: .slug)
        self.source = URL(string: try container.decode(String.self, forKey: .source))
        self.sourcePostURL = try container.decodeIfPresent(String.self, forKey: .sourcePostURL).flatMap({ URL(string: $0) })
        self.sourceTLD = try container.decodeIfPresent(String.self, forKey: .sourceTLD)
        self.title = try container.decode(String.self, forKey: .title)
        self.trendingDateTime = try container.decodeIfPresent(Date.self, forKey: .trendingDateTime)
        self.type = try container.decode(String.self, forKey: .type)
        self.url = try container.decodeIfPresent(String.self, forKey: .url).flatMap({ URL(string: $0) })
        self.user = try container.decodeIfPresent(User.self, forKey: .user)
        self.username = try container.decode(String.self, forKey: .username)
    }
}

public struct Analytics: Decodable {
    public let onload: AnalyticsURL
    public let onclick: AnalyticsURL
    public let onsent: AnalyticsURL
}

public struct AnalyticsURL: Decodable {
    public let url: URL
}

public struct Images: Decodable {
    public let downsized: AnimatedImage?
    public let downsizedLarge: AnimatedImage?
    public let downsizedMedium: AnimatedImage?
    public let downsizedStill: StillImage?
    public let fixedHeight: AnimatedImage?
    public let fixedHeightDownsampled: AnimatedImage?
    public let fixedHeightSmall: AnimatedImage?
    public let fixedHeightSmallStill: StillImage?
    public let fixedHeightStill: StillImage?
    public let fixedWidth: AnimatedImage?
    public let fixedWidthDownsampled: AnimatedImage?
    public let fixedWidthSmall: AnimatedImage?
    public let fixedWidthSmallStill: StillImage?
    public let fixedWidthStill: StillImage?
    public let original: AnimatedImage?
    public let originalMp4: AnimatedImage?
    public let originalStill: StillImage?
    public let preview: AnimatedImage?
    public let previewGif: AnimatedImage?
    public let previewWebp: AnimatedImage?
}

public struct StillImage: Decodable {
    public let url: URL?
    public let width: Int?
    public let height: Int?

    private enum CodingKeys: String, CodingKey {
        case url = "url"
        case width = "width"
        case height = "height"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.url = try container.decodeIfPresent(String.self, forKey: .url).flatMap({ URL(string: $0) })
        self.width = try container.decodeIfPresent(String.self, forKey: .width).flatMap({ Int($0) })
        self.height = try container.decodeIfPresent(String.self, forKey: .height).flatMap({ Int($0) })
    }
}

public struct AnimatedImage: Decodable {
    public let url: URL?
    public let width: Int?
    public let height: Int?
    public let size: Int?
    public let frames: Int?
    public let mp4: URL?
    public let mp4Size: Int?
    public let webp: URL?
    public let webpSize: Int?

    private enum CodingKeys: String, CodingKey {
        case url = "url"
        case width = "width"
        case height = "height"
        case size = "size"
        case frames = "frames"
        case mp4 = "mp4"
        case mp4Size = "mp4_size"
        case webp = "webp"
        case webpSize = "webp_size"
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.url = try container.decodeIfPresent(String.self, forKey: .url).flatMap({ URL(string: $0) })
        self.width = try container.decodeIfPresent(String.self, forKey: .width).flatMap({ Int($0) })
        self.height = try container.decodeIfPresent(String.self, forKey: .height).flatMap({ Int($0) })
        self.size = try container.decodeIfPresent(String.self, forKey: .size).flatMap({ Int($0) })
        self.frames = try container.decodeIfPresent(String.self, forKey: .frames).flatMap({ Int($0) })
        self.mp4 = try container.decodeIfPresent(String.self, forKey: .mp4).flatMap({ URL(string: $0) })
        self.mp4Size = try container.decodeIfPresent(String.self, forKey: .mp4Size).flatMap({ Int($0) })
        self.webp = try container.decodeIfPresent(String.self, forKey: .webp).flatMap({ URL(string: $0) })
        self.webpSize = try container.decodeIfPresent(String.self, forKey: .webpSize).flatMap({ Int($0) })
    }
}
