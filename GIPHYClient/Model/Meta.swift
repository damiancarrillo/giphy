public struct Meta: Decodable {
    public let msg: String
    public let status: Int
    public let responseId: String
}
