//
//  GIPHYClient.h
//  GIPHYClient
//
//  Created by Derek Carrillo on 4/13/19.
//  Copyright © 2019 Damian Carrillo. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for GIPHYClient.
FOUNDATION_EXPORT double GIPHYClientVersionNumber;

//! Project version string for GIPHYClient.
FOUNDATION_EXPORT const unsigned char GIPHYClientVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GIPHYClient/PublicHeader.h>


