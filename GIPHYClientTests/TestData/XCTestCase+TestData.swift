import XCTest

enum TestDataError: Error {
    case missing(fileName: String, ext: String)
}

extension XCTestCase {
    public func loadTestData(fileName: String, ext: String) throws -> Data {
        let testBundle = Bundle(for: type(of: self))
        guard let fileURL = testBundle.url(forResource: fileName, withExtension: ext) else {
            throw TestDataError.missing(fileName: fileName, ext: ext)
        }
        return try Data(contentsOf: fileURL)
    }
}
