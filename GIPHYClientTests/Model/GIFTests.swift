import XCTest

@testable import GIPHYClient

class GIFTests: XCTestCase {

    private var decoder: JSONDecoder!

    override func setUp() {
        super.setUp()

        decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .formatted(Client.dateFormatter)
    }

    func testAnalyticsURLDecoding() throws {
        let data = try loadTestData(fileName: "AnalyticsURL", ext: "json")
        let analyticsURL = try decoder.decode(AnalyticsURL.self, from: data)
        XCTAssertNotNil(analyticsURL)
    }

    func testAnalyticsDecoding() throws {
        let data = try loadTestData(fileName: "Analytics", ext: "json")
        let analytics = try decoder.decode(Analytics.self, from: data)
        XCTAssertNotNil(analytics)
    }

    func testStillImageDecoding() throws {
        let data = try loadTestData(fileName: "StillImage", ext: "json")
        let stillImage = try decoder.decode(StillImage.self, from: data)
        XCTAssertNotNil(stillImage)
    }

    func testAnimatedImageDecoding() throws {
        let data = try loadTestData(fileName: "AnimatedImage", ext: "json")
        let animatedImage = try decoder.decode(AnimatedImage.self, from: data)
        XCTAssertNotNil(animatedImage)
    }

    func testImagesDecoding() throws {
        let data = try loadTestData(fileName: "Images", ext: "json")
        let images = try decoder.decode(Images.self, from: data)
        XCTAssertNotNil(images)
    }

    func testGifDecoding() throws {
        let data = try loadTestData(fileName: "GIF", ext: "json")
        let gif = try decoder.decode(GIF.self, from: data)
        XCTAssertNotNil(gif)
    }

}
