import XCTest

@testable import GIPHYClient

class SearchParametersTests: XCTestCase {

    func testWithQuery() {
        let queryItems = SearchParameters(query: "test").queryItems

        guard queryItems.count == 1 else {
            return XCTFail("Expected 1 query item, got \(queryItems.count)")
        }

        XCTAssertEqual(queryItems.first, URLQueryItem(name: "q", value: "test"))
    }

    func testWithEverything() {
        let queryItems = SearchParameters(query: "test", limit: 5, offset: 0, rating: "g", lang: "en", fmt: "json").queryItems

        guard queryItems.count == 6 else {
            return XCTFail("Expected 6 query items, got \(queryItems.count)")
        }

        XCTAssertEqual(queryItems[0], URLQueryItem(name: "q", value: "test"))
        XCTAssertEqual(queryItems[1], URLQueryItem(name: "limit", value: "5"))
        XCTAssertEqual(queryItems[2], URLQueryItem(name: "offset", value: "0"))
        XCTAssertEqual(queryItems[3], URLQueryItem(name: "rating", value: "g"))
        XCTAssertEqual(queryItems[4], URLQueryItem(name: "lang", value: "en"))
        XCTAssertEqual(queryItems[5], URLQueryItem(name: "fmt", value: "json"))
    }

    func testWithSomeThings() {
        let queryItems = SearchParameters(query: "test", offset: 10, lang: "en").queryItems

        guard queryItems.count == 3 else {
            return XCTFail("Expected 6 query items, got \(queryItems.count)")
        }

        XCTAssertEqual(queryItems[0], URLQueryItem(name: "q", value: "test"))
        XCTAssertEqual(queryItems[1], URLQueryItem(name: "offset", value: "10"))
        XCTAssertEqual(queryItems[2], URLQueryItem(name: "lang", value: "en"))
    }

}
